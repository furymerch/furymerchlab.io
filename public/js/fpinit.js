
$(document).ready(function() {
	$('#fullpage').fullpage({
		//responsiveWidth: 768,
		fixedElements: 'nav',
		controlArrows: true,
		anchors: ['main1', 'info1', 'job1',  'contacts1', 'map1'],
		menu: 'nav',
		scrollOverflow: true,
		scrollBar:true,
		paddingBottom: 80,
		autoScrolling: true,
		
	});
})
$(function(){
	window.setTimeout(function(){
		$('#my-alert').alert('close');
	},5000);
});



function scaleHeader() {
	var scalable = document.querySelectorAll('.scale--js');
	var margin = 10;
	for (var i = 0; i < scalable.length; i++) {
	  var scalableContainer = scalable[i].parentNode;
	  scalable[i].style.transform = 'scale(1)';
	  var scalableContainerWidth = scalableContainer.offsetWidth - margin;
	  var scalableWidth = scalable[i].offsetWidth;
	  scalable[i].style.transform = 'scale(' + scalableContainerWidth / scalableWidth + ')';
	  scalableContainer.style.height = scalable[i].getBoundingClientRect().height + 'px';
	}
  } 
  function debounce(func, wait, immediate) {
	  var timeout;
	  return function() {
		  var context = this, args = arguments;
		  var later = function() {
			  timeout = null;
			  if (!immediate) func.apply(context, args);
		  };
		  var callNow = immediate && !timeout;
		  clearTimeout(timeout);
		  timeout = setTimeout(later, wait);
		  if (callNow) func.apply(context, args);
	  };
  };
  wow = new WOW(
	{
	boxClass:     'wow',      
	animateClass: 'animated', 
	offset:       0,          
	mobile:       false,       
	live:         true        
  }
  )
  wow.init();
  var myScaleFunction = debounce(function() {
	  scaleHeader();
  }, 250);
  
  myScaleFunction();
  $(document).ready(function () {
    $('#fullpage').fullpage();
    $('.fp-prev').append('<span class="fa fa-3x fa-arrow-left"></span>');
    $('.fp-next').append('<span class="fa fa-3x fa-arrow-right"></span>');
});